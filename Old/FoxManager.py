import re
import time
import random
import urllib2
import threading
import xml.etree.ElementTree as ET

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class FoxManager(object):

    def __init__(self, monitizationThreshold, timerText, plusText, links, profile):
        # adds &autoplay=1
        self.plus = plusText
        # Max Timer Setting
        self.sleep = timerText
        # list of links
        self.links = links
        # percentage monitized
        self.monitizationThreshold = monitizationThreshold
        # browser setup
        self.browser = webdriver.Firefox(profile)
        #Cache and cookies
        # Add
        # Add
        #Cache and cookies
        print 'browser is waiting for the proxy to connect'
        browser.implicitly_wait(30)

    def linkSelect(self):
        # selects new random link
        ayylmao = random.choice(self.links)
        link = ayylmao.text
        return link

    def linkSwitch(self, link):
        # moves to the first tab
        browser.find_element_by_tag_name("body").send_keys(
            Keys.CONTROL + Keys.SHIFT + Keys.TAB)
        # CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE
        # CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE
        # V
        # New video
        link = linkSelect(links)
        browser.get(link + plus)

        # Random roll
        random.seed()
        rand2 = random.random()
        rand2 *= 100

        # refreshes the page, 6% likely to do it
        if rand2 < 6:
            browser.refresh()
        browser.find_element_by_tag_name(
            "body").send_keys(Keys.CONTROL + Keys.TAB)

    def main(self):
        plus = self.plus
        sleep = self.sleep
        links = self.links
        browser = self.browser
        monitizationThreshold = self.monitizationThreshold

        # CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE
        # CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE
        # V
        # first load
        browser.get(link + plus)

        # open a tab
        browser.find_element_by_tag_name("body").send_keys(Keys.CONTROL + "t")

        print '--------Loop Start--------'
        # Viewer Vars
        rand = 0
        rand2 = 0
        fullWatch = 0
        adCheck = False
        sleepTime = sleep
        link = ""
        goodPageLoad = False
        reloaded = False
        adQ = False
        # Viewer Loop Start
        # 45 is the amount of videos before the browser closes
        for runCount in range(45):
            # check if page loaded properly
            for pageLoaded in range(5):
                if "playing-mode" in browser.page_source:
                    goodPageLoad = True
                    break
                elif "cued-mode" in browser.page_source:
                    goodPageLoad = True
                    break
                elif "ad-showing" in browser.page_source:
                    goodPageLoad = True
                    adQ = True
                    break
                elif pageLoaded == 4:
                    if reloaded == True:
                        browser.quit()
                    browser.refresh()
                    reloaded = True
                else:
                    time.sleep(1)

            # Watches the whole video
            random.seed()
            fullWatch = random.random() * 100
            if fullWatch > 99:
                while True:
                    if "ended-mode" in browser.page_source:
                        print "watched full video"
                        break
                    time.sleep(5)

            # waits 6 seconds to see if video loads
            for i in range(3):
                # ad loop
                if "ad-showing" in browser.page_source:
                    print "Found AD"
                    adCheck = True
                    # might need to add limit
                    while "ad-showing" in browser.page_source:
                        time.sleep(3)
                    time.sleep(15)
                # no ad found, checks 6 times
                else:
                    print "Ad check " + i
                    time.sleep(2)
                    adQ = False

            # no ad on this page
            if adQ == False:
                random.seed()
                rand = random.random()
                rand *= 100
                if rand < monitizationThreshold:
                    # link selector
                    link = linkSelect()
                    adCheck = False
                    linkSelect(link)
                elif rand > monitizationThreshold:
                    time.sleep(sleepTime)
                    # link selector
                    link = linkSelect()
                    adCheck = False
                    linkSelect(link)
                #
                if adCheck == False:
                    # link selector
                    link = linkSelect()
                    adCheck = False
                    linkSelect(link)
        #Cache and cookies
        browser.quit()
