# -*- coding: utf-8 -*-
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import xml.etree.ElementTree as ET
import unittest
import time
import re
import os
import requests


class TestBot(unittest.TestCase):

    def setUp(self):
        # Checks that user is logged in and a subscriber
        def subAuth(self):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36',
            }
            r = requests.get(
                "http://thebot.net/api/post.php?legacy=0&json", headers=headers)
            if 'Error' in r.text:
                return False
            if 'Subscriber' in r.json()['Usergroups']:
                return True
            return False

        # Returns user's tbn username
        def tbnUsername(self):
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36',
            }
            r = requests.get(
                "http://thebot.net/api/post.php?legacy=0&json", headers=headers)
            if 'Error' in r.text:
                return False
            else:
                return r.json()['Username']

        print "Bot made by AnsemWise"
        print "Donations at https://www.paypal.me/AnsemWise are greatly appreciated,"
        print "but not expected."
        print "Version 1.2"
        subCheck = subAuth(self)
        if subCheck == False:
            print "You need to be logged in to tbn and be a Subscriber."
            raw_input()
            self.fail("Log in to tbn")
        username = tbnUsername(self)
        print "Hello " + username + ", have a nice day."
        raw_input("Press any key to start!")

        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.youtube.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_bot(self):
        tree = ET.ElementTree(file="botSettings.xml")
        root = tree.getroot()
        settings = root.findall("settings")

        email = ""
        password = ""
        adBreaks = ""

        for setting in settings:
            email = setting.find("email").text
            password = setting.find("password").text
            adBreaks = setting.find("adBreaks").text

        driver = self.driver
        driver.get(self.base_url + "")
        driver.find_element_by_xpath("(//button[@type='button'])[2]").click()
        for i in range(60):
            if driver.find_element_by_id("Email"):
                break
            else:
                print "Not found"
            time.sleep(1)
        else:
            self.fail("time out")
        driver.find_element_by_id("Email").clear()
        # time.sleep is put in between some of the clicks so that the page has
        # time to finish loading
        time.sleep(.1)
        driver.find_element_by_id("Email").send_keys(
            email)  # youtube email address
        driver.find_element_by_id("next").click()
        for i in range(60):
            if driver.find_element_by_id("Passwd"):
                break
            else:
                print "Not found"
            time.sleep(1)
        else:
            self.fail("time out")
        driver.find_element_by_id("Passwd").clear()
        time.sleep(.1)
        driver.find_element_by_id("Passwd").send_keys(
            password)  # youtube password
        for i in range(60):
            if driver.find_element_by_id("signIn"):
                break
            else:
                print "Not found"
            time.sleep(1)
        else:
            self.fail("time out")
        driver.find_element_by_id("signIn").click()
        time.sleep(3)
        for elem in tree.iterfind('links/link'):
            # this selects link for video
            vid_id = elem.text
            # this take you to the monitization page of the video from the
            # links above
            driver.get(self.base_url + "edit?o=U&show_mt=1&video_id=" + vid_id)
            print self.base_url + "edit?o=U&show_mt=1&video_id=" + vid_id

            # checks for a popup about donations
            # for i in range(4):
            #     if driver.find_element_by_css_selector("#inproduct-guide-modal > div.iph-dialog-dismiss-container").is_displayed():
            #         driver.find_element_by_xpath(
            #             "//div[@id='inproduct-guide-modal']/div[3]/div[2]/a").click()
            #         print "Fuck donation popup"
            #         driver.get(self.base_url +
            #                    "edit?o=U&show_mt=1&video_id=" + vid_id)
            #         time.sleep(.3)
            #         break
            #     else:
            #         print "Check"
            #         time.sleep(1)
            # else:
            #     print "No donation popup"

            for i in range(60):
                if driver.find_element_by_xpath("//button[@class='yt-uix-button yt-uix-button-size-default yt-uix-button-default ad-break-bulk-insert']"):
                    print "Found"
                    break
                else:
                    print "Ad break button check " + i
                    time.sleep(1)
            else:
                self.fail("time out")

            # opens bulk editor - needs 2 clicks
            driver.find_element_by_xpath(
                "//button[@class='yt-uix-button yt-uix-button-size-default yt-uix-button-default ad-break-bulk-insert']").click()
            driver.find_element_by_xpath(
                "//button[@class='yt-uix-button yt-uix-button-size-default yt-uix-button-default ad-break-bulk-insert']").click()
            time.sleep(.3)

            for i in range(60):
                if driver.find_element_by_xpath("//button[@class='yt-uix-button yt-uix-button-size-default yt-uix-button-primary accept-bulk-insert']"):
                    break
                else:
                    print "Popup not found"
                    driver.find_element_by_xpath(
                        "//button[@class='yt-uix-button yt-uix-button-size-default yt-uix-button-default ad-break-bulk-insert']").click()
                    time.sleep(1)
            else:
                self.fail("time out")

            # Adds ad breaks
            time.sleep(.1)
            driver.find_element_by_css_selector(
                "textarea[name='bulk-insert']").clear()
            time.sleep(.1)
            driver.find_element_by_css_selector(
                "textarea[name='bulk-insert']").send_keys(adBreaks)  # Change ad time here
            time.sleep(.1)

            # sumbit ad changes
            driver.find_element_by_xpath(
                "//button[@class='yt-uix-button yt-uix-button-size-default yt-uix-button-primary accept-bulk-insert']").click()
            time.sleep(.3)

            for i in range(30):
                if "Save changes" == driver.find_element_by_xpath("//button[@class='yt-uix-button yt-uix-button-size-default save-changes-button yt-uix-tooltip yt-uix-button-primary']").text:
                    break
                else:
                    print "Not found"
                    time.sleep(1)

            # save page
            driver.find_element_by_xpath(
                "//button[@class='yt-uix-button yt-uix-button-size-default save-changes-button yt-uix-tooltip yt-uix-button-primary']").click()

            # checks if page finished saving
            for i in range(20):
                if "All changes saved." == driver.find_element_by_css_selector("span.save-error-message").text:
                    break
                else:
                    print "Not found"
                time.sleep(1)
            time.sleep(1)

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException, e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
