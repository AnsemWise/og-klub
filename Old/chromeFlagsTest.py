import os
import re
import sys
import time
import urllib2
import threading
import xml.etree.ElementTree as ET

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# tree = ET.ElementTree(file="settings.xml")
# root = tree.getroot()
# userData = root.findall(user)
# tutu = ""
# web = ""

link = "https://www.youtube.com/watch?v=n8AZ6vEl8Tk"

# chromedriver = "/Users/Alex/Documents/og-klub/chromedriver"
# os.environ["webdriver.chrome.driver"] = chromedriver

# for data in userData:
# tutu = data.find("tutu").text
# web = data.find("websyndic").text


def main():
    chrome_options = Options()
    chrome_options.add_argument("--aggressive-cache-discard")
    chrome_options.add_argument("--conservative")
    chrome_options.add_argument("--crash-on-failure")
    chrome_options.add_argument("--crash-on-hang-threads")
    chrome_options.add_argument("--disable-3d-apis")
    chrome_options.add_argument("--disable-about-in-settings")
    chrome_options.add_argument("--disable-accelerated-2d-canvas")
    chrome_options.add_argument("--disable-accelerated-jpeg-decoding")
    chrome_options.add_argument("--disable-accelerated-mjpeg-decode")
    chrome_options.add_argument("--disable-accelerated-video-decode")
    chrome_options.add_argument("--disable-account-consistency")
    chrome_options.add_argument("--disable-add-to-shelf")
    chrome_options.add_argument("--disable-affiliation-based-matching")
    chrome_options.add_argument(
        "--disable-android-compositor-animation-timelines[7]")
    chrome_options.add_argument("--disable-app-info-dialog-mac[4]")
    chrome_options.add_argument("--disable-app-list-dismiss-on-blur")
    chrome_options.add_argument("--disable-app-window-cycling[4]")
    chrome_options.add_argument("--disable-appcontainer")
    chrome_options.add_argument("--disable-async-dns")
    chrome_options.add_argument("--disable-auto-hiding-toolbar-threshold[7]")
    chrome_options.add_argument(
        "--disable-autofill-keyboard-accessory-view[7]")
    chrome_options.add_argument("--disable-autofill-save-card-bubble")
    chrome_options.add_argument("--disable-background-networking")
    chrome_options.add_argument("--disable-background-timer-throttling")
    chrome_options.add_argument("--disable-backing-store-limit")
    chrome_options.add_argument("--disable-blink-features")
    chrome_options.add_argument("--disable-boot-animation")
    chrome_options.add_argument("--disable-breakpad")
    chrome_options.add_argument("--disable-bundled-ppapi-flash")
    chrome_options.add_argument("--disable-cache")
    chrome_options.add_argument("--disable-cached-picture-raster")
    chrome_options.add_argument("--disable-canvas-aa")
    chrome_options.add_argument("--disable-captive-portal-bypass-proxy")
    chrome_options.add_argument("--disable-cast-streaming-hw-encoding")
    chrome_options.add_argument("--disable-child-account-detection")
    chrome_options.add_argument("--disable-clear-browsing-data-counters")
    chrome_options.add_argument("--disable-client-side-phishing-detection")
    chrome_options.add_argument("--disable-cloud-import")
    chrome_options.add_argument("--disable-component-cloud-policy")
    chrome_options.add_argument(
        "--disable-component-extensions-with-background-pages")
    chrome_options.add_argument("--disable-component-update")
    chrome_options.add_argument("--disable-composited-antialiasing")
    chrome_options.add_argument("--disable-compositor-animation-timelines")
    chrome_options.add_argument("--disable-compositor-property-trees")
    chrome_options.add_argument("--disable-confirmation")
    chrome_options.add_argument("--disable-contextual-search")
    chrome_options.add_argument("--disable-core-animation-plugins[9]")
    chrome_options.add_argument("--disable-credit-card-scan")
    chrome_options.add_argument("--disable-d3d11")
    chrome_options.add_argument("--disable-databases")
    chrome_options.add_argument("--disable-datasaver-prompt")
    chrome_options.add_argument("--disable-default-apps")
    chrome_options.add_argument("--disable-delay-agnostic-aec")
    chrome_options.add_argument("--disable-demo-mode")
    chrome_options.add_argument("--disable-device-disabling")
    chrome_options.add_argument("--disable-device-discovery-notifications")
    chrome_options.add_argument("--disable-dinosaur-easter-egg")
    chrome_options.add_argument("--disable-direct-composition")
    chrome_options.add_argument("--disable-direct-npapi-requests")
    chrome_options.add_argument("--disable-direct-write[5]")
    chrome_options.add_argument("--disable-directwrite-for-ui[5]")
    chrome_options.add_argument("--disable-display-color-calibration[6]")
    chrome_options.add_argument("--disable-display-list-2d-canvas")
    chrome_options.add_argument("--disable-distance-field-text")
    chrome_options.add_argument("--disable-domain-blocking-for-3d-apis")
    chrome_options.add_argument("--disable-domain-reliability")
    chrome_options.add_argument("--disable-download-notification")
    chrome_options.add_argument("--disable-drive-search-in-app-launcher")
    chrome_options.add_argument("--disable-drop-sync-credential")
    chrome_options.add_argument("--disable-dwm-composition")
    chrome_options.add_argument("--disable-encrypted-media")
    chrome_options.add_argument("--disable-experimental-app-list")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-extensions-file-access-check")
    chrome_options.add_argument("--disable-extensions-http-throttling")
    chrome_options.add_argument("--disable-fast-web-scroll-view-insets")
    chrome_options.add_argument("--disable-features")
    chrome_options.add_argument("--disable-field-trial-config")
    chrome_options.add_argument("--disable-file-system")
    chrome_options.add_argument("--disable-fill-on-account-select")
    chrome_options.add_argument("--disable-flash-3d")
    chrome_options.add_argument("--disable-flash-stage3d")
    chrome_options.add_argument("--disable-full-form-autofill-ios")
    chrome_options.add_argument("--disable-gaia-services")
    chrome_options.add_argument("--disable-gesture-editing")
    chrome_options.add_argument(
        "--disable-gesture-requirement-for-media-playback")
    chrome_options.add_argument("--disable-gesture-typing")
    chrome_options.add_argument("--disable-gl-drawing-for-tests")
    chrome_options.add_argument("--disable-gl-error-limit")
    chrome_options.add_argument("--disable-gl-extensions")
    chrome_options.add_argument("--disable-glsl-translator")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--disable-gpu-compositing")
    chrome_options.add_argument("--disable-gpu-driver-bug-workarounds")
    chrome_options.add_argument("--disable-gpu-early-init")
    chrome_options.add_argument(
        "--disable-gpu-memory-buffer-compositor-resources")
    chrome_options.add_argument("--disable-gpu-memory-buffer-video-frames")
    chrome_options.add_argument("--disable-gpu-process-crash-limit")
    chrome_options.add_argument("--disable-gpu-program-cache")
    chrome_options.add_argument("--disable-gpu-rasterization")
    chrome_options.add_argument("--disable-gpu-sandbox")
    chrome_options.add_argument("--disable-gpu-shader-disk-cache")
    chrome_options.add_argument("--disable-gpu-vsync")
    chrome_options.add_argument("--disable-gpu-watchdog")
    chrome_options.add_argument("--disable-hang-monitor")
    chrome_options.add_argument("--disable-hid-detection-on-oobe")
    chrome_options.add_argument(
        "--disable-hide-inactive-stacked-tab-close-buttons")
    chrome_options.add_argument("--disable-histogram-customizer")
    chrome_options.add_argument("--disable-hosted-app-shim-creation[4]")
    chrome_options.add_argument("--disable-hosted-apps-in-windows[4]")
    chrome_options.add_argument("--disable-icon-ntp")
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--disable-input-view")
    chrome_options.add_argument("--disable-ios-password-generation")
    chrome_options.add_argument("--disable-ios-password-suggestions")
    chrome_options.add_argument("--disable-ipv4")
    chrome_options.add_argument("--disable-ipv6")
    chrome_options.add_argument("--disable-javascript-harmony-shipping")
    chrome_options.add_argument("--disable-kill-after-bad-ipc")
    chrome_options.add_argument("--disable-lcd-text")
    chrome_options.add_argument("--disable-legacy-window[5]")
    chrome_options.add_argument("--disable-local-storage")
    chrome_options.add_argument("--disable-logging")
    chrome_options.add_argument("--disable-login-animations")
    chrome_options.add_argument("--disable-low-end-device-mode")
    chrome_options.add_argument("--disable-low-res-tiling")
    chrome_options.add_argument("--disable-lru-snapshot-cache")
    chrome_options.add_argument("--disable-mac-overlays[9]")
    chrome_options.add_argument("--disable-mac-views-native-app-windows[4]")
    chrome_options.add_argument("--disable-main-frame-before-activation")
    chrome_options.add_argument("--disable-manager-for-sync-signin")
    chrome_options.add_argument("--disable-md-downloads")
    chrome_options.add_argument("--disable-media-source")
    chrome_options.add_argument("--disable-media-suspend")
    chrome_options.add_argument("--disable-media-thread-for-media-playback[7]")
    chrome_options.add_argument("--disable-merge-key-char-events[5]")
    chrome_options.add_argument("--disable-method-check")
    chrome_options.add_argument(
        "--disable-minimize-on-second-launcher-item-click")
    chrome_options.add_argument("--disable-mojo-channel")
    chrome_options.add_argument("--disable-mtp-write-support")
    chrome_options.add_argument("--disable-multilingual-spellchecker[10]")
    chrome_options.add_argument("--disable-namespace-sandbox")
    chrome_options.add_argument("--disable-native-gpu-memory-buffers")
    chrome_options.add_argument("--disable-network-portal-notification")
    chrome_options.add_argument("--disable-new-app-list-mixer")
    chrome_options.add_argument("--disable-new-bookmark-apps")
    chrome_options.add_argument("--disable-new-channel-switcher-ui")
    chrome_options.add_argument("--disable-new-kiosk-ui")
    chrome_options.add_argument("--disable-new-korean-ime")
    chrome_options.add_argument("--disable-new-profile-management")
    chrome_options.add_argument("--disable-new-task-manager[11]")
    chrome_options.add_argument("--disable-new-zip-unpacker")
    chrome_options.add_argument("--disable-notifications")
    chrome_options.add_argument("--disable-ntp-favicons")
    chrome_options.add_argument("--disable-ntp-popular-sites")
    chrome_options.add_argument("--disable-offer-store-unmasked-wallet-cards")
    chrome_options.add_argument("--disable-offer-upload-credit-cards")
    chrome_options.add_argument("--disable-office-editing-component-extension")
    chrome_options.add_argument("--disable-offline-auto-reload")
    chrome_options.add_argument("--disable-offline-auto-reload-visible-only")
    chrome_options.add_argument("--disable-offline-pages")
    chrome_options.add_argument("--disable-out-of-process-pac")
    chrome_options.add_argument("--disable-overlay-scrollbar")
    chrome_options.add_argument("--disable-overscroll-edge-effect[7]")
    chrome_options.add_argument("--disable-page-visibility")
    chrome_options.add_argument("--disable-panel-fitting[6]")
    chrome_options.add_argument("--disable-panels")
    chrome_options.add_argument("--disable-password-generation")
    chrome_options.add_argument("--disable-password-manager-reauthentication")
    chrome_options.add_argument("--disable-password-separated-signin-flow")
    chrome_options.add_argument("--disable-pepper-3d")
    chrome_options.add_argument("--disable-permissions-api")
    chrome_options.add_argument("--disable-permissions-blacklist")
    chrome_options.add_argument("--disable-physical-keyboard-autocorrect")
    chrome_options.add_argument("--disable-pinch")
    chrome_options.add_argument("--disable-plugins-discovery")
    chrome_options.add_argument("--disable-pnacl-crash-throttling")
    chrome_options.add_argument("--disable-policy-key-verification")
    chrome_options.add_argument("--disable-popup-blocking")
    chrome_options.add_argument("--disable-preconnect")
    chrome_options.add_argument("--disable-prefer-compositing-to-lcd-text")
    chrome_options.add_argument("--disable-presentation-api")
    chrome_options.add_argument("--disable-print-preview")
    chrome_options.add_argument("--disable-prompt-on-repost")
    chrome_options.add_argument("--disable-pull-to-refresh-effect[7]")
    chrome_options.add_argument("--disable-push-api-background-mode")
    chrome_options.add_argument("--disable-quic")
    chrome_options.add_argument("--disable-quic-port-selection")
    chrome_options.add_argument("--disable-reading-from-canvas")
    chrome_options.add_argument("--disable-remote-core-animation[9]")
    chrome_options.add_argument("--disable-remote-fonts")
    chrome_options.add_argument("--disable-renderer-accessibility")
    chrome_options.add_argument("--disable-renderer-backgrounding")
    chrome_options.add_argument("--disable-rgba-4444-textures")
    chrome_options.add_argument("--disable-rollback-option")
    chrome_options.add_argument("--disable-rtc-smoothness-algorithm")
    chrome_options.add_argument("--disable-save-password-bubble[4]")
    chrome_options.add_argument("--disable-screen-orientation-lock[7]")
    chrome_options.add_argument("--disable-seccomp-filter-sandbox")
    chrome_options.add_argument("--disable-session-crashed-bubble")
    chrome_options.add_argument("--disable-settings-window")
    chrome_options.add_argument("--disable-setuid-sandbox")
    chrome_options.add_argument("--disable-shader-name-hashing")
    chrome_options.add_argument("--disable-shared-workers")
    chrome_options.add_argument("--disable-signin-scoped-device-id")
    chrome_options.add_argument("--disable-simplified-fullscreen-ui")
    chrome_options.add_argument("--disable-single-click-autofill")
    chrome_options.add_argument("--disable-site-engagement-service")
    chrome_options.add_argument("--disable-smart-virtual-keyboard")
    chrome_options.add_argument("--disable-smooth-scrolling")
    chrome_options.add_argument("--disable-software-rasterizer")
    chrome_options.add_argument("--disable-spdy-proxy-dev-auth-origin")
    chrome_options.add_argument("--disable-speech-api")
    chrome_options.add_argument("--disable-sync")
    chrome_options.add_argument("--disable-sync-app-list")
    chrome_options.add_argument("--disable-sync-backup")
    chrome_options.add_argument("--disable-sync-rollback")
    chrome_options.add_argument("--disable-sync-types")
    chrome_options.add_argument("--disable-tab-eviction")
    chrome_options.add_argument("--disable-tab-switcher")
    chrome_options.add_argument("--disable-threaded-animation")
    chrome_options.add_argument("--disable-threaded-compositing")
    chrome_options.add_argument("--disable-threaded-scrolling")
    chrome_options.add_argument("--disable-timezone-tracking-option")
    chrome_options.add_argument("--disable-touch-adjustment")
    chrome_options.add_argument("--disable-touch-drag-drop")
    chrome_options.add_argument("--disable-touch-feedback")
    chrome_options.add_argument("--disable-translate")
    chrome_options.add_argument("--disable-translate-new-ux[4]")
    chrome_options.add_argument("--disable-usb-keyboard-detect[5]")
    chrome_options.add_argument("--disable-v8-idle-tasks")
    chrome_options.add_argument("--disable-vaapi-accelerated-video-encode[6]")
    chrome_options.add_argument("--disable-views-rect-based-targeting")
    chrome_options.add_argument("--disable-virtual-keyboard-overscroll")
    chrome_options.add_argument("--disable-voice-input")
    chrome_options.add_argument("--disable-volume-adjust-sound")
    chrome_options.add_argument("--disable-wake-on-wifi")
    chrome_options.add_argument("--disable-web-notification-custom-layouts")
    chrome_options.add_argument("--disable-web-resources")
    chrome_options.add_argument("--disable-web-security")
    chrome_options.add_argument("--disable-webaudio")
    chrome_options.add_argument("--disable-webgl")
    chrome_options.add_argument("--disable-webrtc-encryption[12]")
    chrome_options.add_argument("--disable-webrtc-hw-decoding[12]")
    chrome_options.add_argument("--disable-webrtc-hw-encoding[12]")
    chrome_options.add_argument("--disable-win32k-renderer-lockdown[5]")
    chrome_options.add_argument("--disable-wkwebview")
    chrome_options.add_argument("--disable-x-token")
    chrome_options.add_argument("--disable-xss-auditor")
    chrome_options.add_argument("--disable-zero-browsers-open-for-tests")
    chrome_options.add_argument("--disable-zero-copy")
    chrome_options.add_argument("--disabled")
    chrome_options.add_argument("--incognito")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--single-process")
    chrome_options.add_argument("--web-worker-share-processes")

    print 'starting the main func'
    # var
    browser = webdriver.Chrome(chrome_options=chrome_options)

    print 'browser.implicitly_wait(30)'
    browser.implicitly_wait(30)

    # tutuhits
    print 'browser.get(' + link + ')'
    browser.get(link)

    # tutuhits check if page is loaded
    if browser.page_source is None:
        browser22.quit()
        print "Error"
        main()

    # Wait for things to load
    print 'time.sleep(7)'
    time.sleep(7)

    timer = 60

    print 'Loop Start'
    while True:
        # tutuhits
        if timer >= 0:
            timer -= 1
            print "Timer Count Down: " + str(timer)
            time.sleep(.99)
        else:
            browser.get(link)

main()

print "Done"
