import re
import time
import random
import urllib2
import threading
from og - klub import FoxManager
import xml.etree.ElementTree as ET

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# load arrays
tree = ET.ElementTree(file="settings.xml")
root = tree.getroot()
profile = root.findall("profile")
timerText = root.findall("maxtimer")
plusText = root.findall("plus")
userAgents = root.findall("ua")
proxies = root.findall("proxy")
referrers = root.findall("ref")
links = root.findall("link")
monthreshold = root.findall("monthreshold")
viewerAmount = root.findall("viewerAmount")

monthreshold = monthreshold[0].text
viewerAmount = viewerAmount[0].text
profile = profile[0].text

profile = webdriver.FirefoxProfile(profile)

# general
profile.set_preference('browser.xul.error_pages.enabled', "false")
profile.set_preference('extensions.blocklist.enabled', "false")
# playback
profile.set_preference('media.mediasource.enabled', "true")
profile.set_preference('media.gmp-provider.enabled', "true")
profile.set_preference('media.mediasource.mp4.enabled', "true")
profile.set_preference('media.fragmented-mp4.*', "true")
profile.set_preference(
    'media.fragmented-mp4.use-blank-decoder', "true")
profile.set_preference('media.mediasource.webm.enabled', "true")
profile.set_preference('media.autoplay.enabled', "true")
# performance
profile.set_preference('browser.sessionhistory.max_total_viewer', 0)
profile.set_preference('network.http.pipelining', "true")
profile.set_preference('network.http.proxy.pipelining', "true")
profile.set_preference('network.http.pipelining.maxrequests', 20)
profile.set_preference('network.http.pipelining.ssl', "false")
profile.set_preference('network.http.max-connections', 45)
profile.set_preference('network.http.max-connections-per-server', 30)
profile.set_preference('nglayout.initialpaint.delay', 0)
profile.set_preference('network.dns.disableIPv6', "false")
profile.set_preference('content.notify.backoffcount', 5)
profile.set_preference('plugin.expose_full_path', "true")
profile.set_preference('config.trim_on_minimize', "true")
profile.set_preference('content.notify.backoffcount', 5)
profile.set_preference('content.notify.interval', 849999)
profile.set_preference('gfx.direct2d.disabled', "false")
profile.set_preference('layers.acceleration.disabled', "false")
profile.set_preference('dom.ipc.processCount', 3)
# network
profile.set_preference('network.proxy.share_proxy_settings', "true")
# CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE
# V
profile.set_preference('network.proxy.type', 0)


def setAccount(profile):
    plusText[0].text
    ansemrocks = random.choice(userAgents)
    useragentsel = ansemrocks.text
    # proxy selector
    money = random.choice(proxies)
    proxysel = money.text
    proxysel = proxysel.split(':', 1)
    print proxysel[0] + " " + proxysel[1]
    port = int(proxysel[1])
    proxies.remove(money)

    # referrer selector
    coolcat = random.choice(referrers)
    referrersel = '@DEFAULT=' + coolcat.text

    # link selector
    ayylmao = random.choice(links)
    link = ayylmao.text

    random.seed()
    rand = random.random()
    rand *= 100

    # chooses when to set ref
    if(rand < 92):
        # sets ref
        profile.set_preference('refcontrol.actions', referrersel)
    profile.set_preference('general.useragent.override', useragentsel)
    profile.set_preference('network.proxy.http_port', port)
    profile.set_preference('network.proxy.http', proxysel[0])
    return profile

thread_list = []

for count in range(10):  # Viewers Running
    foxManager = FoxManager(monthreshold,
                            timerText, plusText, links, setAccount(profile))
    t1 = threading.Thread(target=foxManager.main, args=())
    thread_list.append(t1)

for thread in thread_list:
    thread.start()

for thread in thread_list:
    thread.join()
