# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
import time
import re
import random


class hitsLinkRotator(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://22hits.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_hits_link_rotator(self):
        driver = self.driver
        driver.get(self.base_url + "/index.php")
        driver.find_element_by_id("usr_email").click()
        driver.find_element_by_id("usr_email").clear()
        driver.find_element_by_id("usr_email").send_keys(
            "jackofspades707@hotmail.com")  # login E-mail
        driver.find_element_by_id("usr_pass").clear()
        driver.find_element_by_id("usr_pass").send_keys(
            "crimson")  # login Password
        driver.find_element_by_name("login").click()
        driver.find_element_by_link_text("My links").click()
        driver.find_element_by_css_selector("#slot_91455 > #url").clear()
        driver.find_element_by_css_selector(
            "#slot_91455 > #url").send_keys(RNGlink)
        driver.find_element_by_css_selector("#slot_91456 > #url").clear()
        driver.find_element_by_css_selector(
            "#slot_91456 > #url").send_keys(RNGlink)
        driver.find_element_by_css_selector("#slot_91455 > a").click()
        driver.find_element_by_css_selector("#slot_91456 > a").click()
        driver.find_element_by_link_text("Save").click()

        def RNGlink():
            RNG = random.shuffle(linklist)
            linklist = '''
	https://www.youtube.com/watch?v=B2MnaZnS3F4
	https://www.youtube.com/watch?v=4jTHWlw-260
	https://www.youtube.com/watch?v=U2CLFIfyJKU
	https://www.youtube.com/watch?v=9EMR7utyiwc
	https://www.youtube.com/watch?v=vAK6VBTltUw
	https://www.youtube.com/watch?v=yOJLIHAXWi8
	https://www.youtube.com/watch?v=GEycA_bCPW4
	https://www.youtube.com/watch?v=f1PgyU2PmFs
	https://www.youtube.com/watch?v=sqU9Fb8IT4U
	https://www.youtube.com/watch?v=uNW-wLsQeLY
	https://www.youtube.com/watch?v=va7XkWIIDuc
	https://www.youtube.com/watch?v=uSaGed_prBA
	https://www.youtube.com/watch?v=tkOYmJG1qEA
	https://www.youtube.com/watch?v=J3fKdD_eq14
	https://www.youtube.com/watch?v=wV9z6ieN1Ss
	https://www.youtube.com/watch?v=PFYWIKejzUk
	https://www.youtube.com/watch?v=OjTPSj_gAHA
	https://www.youtube.com/watch?v=KVFQMis69V8
	https://www.youtube.com/watch?v=OqcAWCqsLNk
	https://www.youtube.com/watch?v=snOV6OIzwto
	https://www.youtube.com/watch?v=PCuB6RtDZLI
	https://www.youtube.com/watch?v=meggZDQ5OY8
	https://www.youtube.com/watch?v=ioC68zuMjLI
	https://www.youtube.com/watch?v=Az6h16k0q70
	https://www.youtube.com/watch?v=Q1RYu3Nfz_o
	https://www.youtube.com/watch?v=z1BGWVW29XU
	https://www.youtube.com/watch?v=SUCYIrhTxQk
	https://www.youtube.com/watch?v=M_HrNw0a7XQ
	https://www.youtube.com/watch?v=eQQQr1SlTys
	https://www.youtube.com/watch?v=kryzemv-Fog
	https://www.youtube.com/watch?v=WCoD2y1JU4M
	https://www.youtube.com/watch?v=yExcFt52YHI
	https://www.youtube.com/watch?v=IbamnSkuqPs
	https://www.youtube.com/watch?v=FJcZI_FjbQc
	https://www.youtube.com/watch?v=41FLkqkNXiI
	https://www.youtube.com/watch?v=PpWOVNRATx8
	https://www.youtube.com/watch?v=23q_94dDPNo
	https://www.youtube.com/watch?v=Wbz6gHe5f4Y
	https://www.youtube.com/watch?v=0vb-Nd5-t3w
	https://www.youtube.com/watch?v=V0atUekXqn4
	https://www.youtube.com/watch?v=HHJ7gopHJ7A
	https://www.youtube.com/watch?v=jil8mRfa43w
	https://www.youtube.com/watch?v=vQDdwrpgSLI
	https://www.youtube.com/watch?v=oFOz_9LnoGU
	https://www.youtube.com/watch?v=Ow2rKL_9dt8
	https://www.youtube.com/watch?v=o3-kRQj6gak
	https://www.youtube.com/watch?v=yKjxQj7Cowo
	https://www.youtube.com/watch?v=dXNGFp4geqQ
https://www.youtube.com/watch?v=WMuI7Y6ITnc
https://www.youtube.com/watch?v=TjXBu6pN4lE
https://www.youtube.com/watch?v=760T3C1Bh7U
https://www.youtube.com/watch?v=STcacP54sgk
https://www.youtube.com/watch?v=et1HyxnSH-k
https://www.youtube.com/watch?v=15LedfIJccQ
https://www.youtube.com/watch?v=hLrNU9_o1x4
https://www.youtube.com/watch?v=-i_ZEplqA6Y
https://www.youtube.com/watch?v=rT5H0xUT0ms
https://www.youtube.com/watch?v=XjkUS1PHlTk
https://www.youtube.com/watch?v=cN1eDmeKDZs
https://www.youtube.com/watch?v=3m3WDY5QMrc
https://www.youtube.com/watch?v=wpXVMR3wtzc
https://www.youtube.com/watch?v=FeVbofnXONo
https://www.youtube.com/watch?v=KA06wiDfI6Y
https://www.youtube.com/watch?v=1A0RoBc93uc
https://www.youtube.com/watch?v=KBu7bDG6-3g
https://www.youtube.com/watch?v=1gdxgoTsGBU
https://www.youtube.com/watch?v=Es3vXe9BV2w
https://www.youtube.com/watch?v=nQ-SZKeS33o
https://www.youtube.com/watch?v=GyZUppW2xnY
https://www.youtube.com/watch?v=SIIgl_uZnpU
https://www.youtube.com/watch?v=ZJL6hATph18
https://www.youtube.com/watch?v=-MA-wAuZNR8
https://www.youtube.com/watch?v=LHxKPoTez_U
https://www.youtube.com/watch?v=zj3gvuNHtJI
https://www.youtube.com/watch?v=t4q88Zfrgaw
https://www.youtube.com/watch?v=trNIOcVSaTo
https://www.youtube.com/watch?v=cXNXtXkazQA
https://www.youtube.com/watch?v=YycxqkWd--E
https://www.youtube.com/watch?v=3Hb66ijD9qM
https://www.youtube.com/watch?v=YGGkRyvuVi4
https://www.youtube.com/watch?v=dSDH9w8UsSU
https://www.youtube.com/watch?v=MeNnMyQhj70
https://www.youtube.com/watch?v=kbby0bBTebQ
https://www.youtube.com/watch?v=hUZDZkwNwr4
https://www.youtube.com/watch?v=L4qd-Zpmcdg
https://www.youtube.com/watch?v=1M9xvoCk9FM
https://www.youtube.com/watch?v=bbcruZNnO2U
https://www.youtube.com/watch?v=_HWuimrnfxc
https://www.youtube.com/watch?v=oBA-8XT0CLg
https://www.youtube.com/watch?v=GiEPiMkhHnA
https://www.youtube.com/watch?v=q9J-Td5jEMQ
https://www.youtube.com/watch?v=t2ZgyHXmuDA
https://www.youtube.com/watch?v=5erlNna-SJQ
https://www.youtube.com/watch?v=qXoinvh_gQ4
https://www.youtube.com/watch?v=_e2vfUQwh3E
https://www.youtube.com/watch?v=-BjcWJf40q0
https://www.youtube.com/watch?v=ZrM0giXBi3s
https://www.youtube.com/watch?v=nQQNfF8Lyzc
https://www.youtube.com/watch?v=XbkIdyr7C9I
https://www.youtube.com/watch?v=at6lZBEW2UE
https://www.youtube.com/watch?v=rLM1rkk_UyE
https://www.youtube.com/watch?v=59PLk7Eh6Pk
https://www.youtube.com/watch?v=TYBVXFvtdbA
https://www.youtube.com/watch?v=UtiMhhEv8Os
https://www.youtube.com/watch?v=RlRQkvKgf_g
https://www.youtube.com/watch?v=dQhNNwWWlQk
https://www.youtube.com/watch?v=klCnZMVx-IE
https://www.youtube.com/watch?v=2eAGpXCk2dU
https://www.youtube.com/watch?v=p85uMjMizdY
https://www.youtube.com/watch?v=HuC0L2clRZs
https://www.youtube.com/watch?v=cMYl7cqoQsM
https://www.youtube.com/watch?v=aGTC8cN4pYU
https://www.youtube.com/watch?v=gdqrp6aJzdY
https://www.youtube.com/watch?v=puGOX2Ze_cI
https://www.youtube.com/watch?v=l8wXcsZm_3s
https://www.youtube.com/watch?v=Q3NKMMYoe7s
https://www.youtube.com/watch?v=A1w7t6XmdlE
https://www.youtube.com/watch?v=E_xmphPdb5A
https://www.youtube.com/watch?v=feCr_0cwd4s
https://www.youtube.com/watch?v=BSBN5NSFkxo
https://www.youtube.com/watch?v=J2ZGad3yHyI
https://www.youtube.com/watch?v=u_I-hx5kSJI
https://www.youtube.com/watch?v=Km5z7O9NRpc
https://www.youtube.com/watch?v=eOfPXf6O5Po
https://www.youtube.com/watch?v=Jdzql7UDCcQ
https://www.youtube.com/watch?v=NXJKewv6xts
https://www.youtube.com/watch?v=CDY5k-q4wxY
https://www.youtube.com/watch?v=pGuzuAcUeOY
https://www.youtube.com/watch?v=MU5-7Tqcb5w
https://www.youtube.com/watch?v=32DA1lMwmAU
https://www.youtube.com/watch?v=vfSbYYD47YM
https://www.youtube.com/watch?v=XS1ZmfMf28o
https://www.youtube.com/watch?v=Ul2-RHiBg9I
https://www.youtube.com/watch?v=e8mhaoWAoFM
https://www.youtube.com/watch?v=f8Xc0gWo52w
https://www.youtube.com/watch?v=dbxKVVYpcaU
https://www.youtube.com/watch?v=sWOhHhdOgpA
https://www.youtube.com/watch?v=aHhvs7WDDC0
https://www.youtube.com/watch?v=Kjzf4W7g2L8
https://www.youtube.com/watch?v=KH2CvXobPV0
https://www.youtube.com/watch?v=SyptufBQ1PE
https://www.youtube.com/watch?v=SbZu581iBZg
https://www.youtube.com/watch?v=uBWNiOF0YMM
https://www.youtube.com/watch?v=6DaDq8hnSzM
https://www.youtube.com/watch?v=NTDP04Ddlvw
https://www.youtube.com/watch?v=sVZ3vPupiu4
https://www.youtube.com/watch?v=6zYF8a_R0qQ
https://www.youtube.com/watch?v=mmSsPsBsMtw
https://www.youtube.com/watch?v=mrcCjaHMBDM
https://www.youtube.com/watch?v=26ZI7kwlqcM
https://www.youtube.com/watch?v=VYA20GheI3U
https://www.youtube.com/watch?v=h7SfTervWJo
https://www.youtube.com/watch?v=RMslZM6QN2I
https://www.youtube.com/watch?v=bAkgrQN6tgs
https://www.youtube.com/watch?v=wwqx5LKp2uc
https://www.youtube.com/watch?v=Vvof5_7X-OQ
https://www.youtube.com/watch?v=KlTFZfAeDrw
https://www.youtube.com/watch?v=SBQUvq2ZYfU
https://www.youtube.com/watch?v=W3hMg3rJFmg
https://www.youtube.com/watch?v=lJ7NUzFl8Es
https://www.youtube.com/watch?v=UFqUunxUzgM
https://www.youtube.com/watch?v=JSxFJnYGA_c
https://www.youtube.com/watch?v=dldKY4xLggo
https://www.youtube.com/watch?v=j8NfFaESYlU
https://www.youtube.com/watch?v=gIWck_UkmpY
https://www.youtube.com/watch?v=T1fcdSaHQsk
https://www.youtube.com/watch?v=vD9Ca8vj4Yg
https://www.youtube.com/watch?v=uWRuDbZfh48
https://www.youtube.com/watch?v=jhU7tHk75MQ
https://www.youtube.com/watch?v=VzZ8kALjUw4
https://www.youtube.com/watch?v=ETchqtThzgI
https://www.youtube.com/watch?v=JWQFm2KrPnY
https://www.youtube.com/watch?v=Jik6MUphiFU
https://www.youtube.com/watch?v=ElMgAm6q1Cg
https://www.youtube.com/watch?v=BVC3Y3hQqxc
https://www.youtube.com/watch?v=AVxxsAAFXgI
https://www.youtube.com/watch?v=IjWZyR5KnVk
https://www.youtube.com/watch?v=TploeClaupU
https://www.youtube.com/watch?v=5LH3hF8fzys
https://www.youtube.com/watch?v=z06_n75rsiQ
https://www.youtube.com/watch?v=BTaCDlAgAB4
https://www.youtube.com/watch?v=K8ak4XLHD1Y
https://www.youtube.com/watch?v=oJ2AKfaXOPY
https://www.youtube.com/watch?v=NvlMFjhpZJE
https://www.youtube.com/watch?v=xzMa1GRjCyA
https://www.youtube.com/watch?v=C-KXU-fcZiY
https://www.youtube.com/watch?v=kzmc1yfVcso
https://www.youtube.com/watch?v=B2XzNuOfV9w
https://www.youtube.com/watch?v=COrsShlplJE
https://www.youtube.com/watch?v=EKZa6yBp54U
https://www.youtube.com/watch?v=cCmKKO1LuMk
https://www.youtube.com/watch?v=_zG8ZwB6J2M
https://www.youtube.com/watch?v=gJkzsguymRo
https://www.youtube.com/watch?v=BEzZIddgzm4
https://www.youtube.com/watch?v=f3-zFZZLJ4s
https://www.youtube.com/watch?v=EXiKIKvWU0w
https://www.youtube.com/watch?v=acp3zeGwWzg
https://www.youtube.com/watch?v=uaG64tifnYc
https://www.youtube.com/watch?v=b3po1fJbMVY
https://www.youtube.com/watch?v=C7ETq-DK4aU
https://www.youtube.com/watch?v=awVoeVGt464
https://www.youtube.com/watch?v=HSFkrwQhMOo
https://www.youtube.com/watch?v=44uOAQpMaqw
https://www.youtube.com/watch?v=7JDciRfIlws
https://www.youtube.com/watch?v=BKCNnhj_UFw
https://www.youtube.com/watch?v=kSGYkokoq08
https://www.youtube.com/watch?v=2tes77jens8
https://www.youtube.com/watch?v=ZeUD4h9md8I
https://www.youtube.com/watch?v=plw98EID6dY
https://www.youtube.com/watch?v=VKoxDV7JQe8
https://www.youtube.com/watch?v=49-X5__q5Fk
https://www.youtube.com/watch?v=vgB1GD_ud_c
https://www.youtube.com/watch?v=W8Wx31p7lco
https://www.youtube.com/watch?v=n_gGd8WnCDw
https://www.youtube.com/watch?v=gi53_Eqic0k
https://www.youtube.com/watch?v=bBP_GNcG7jg
https://www.youtube.com/watch?v=UtS8gtDVoe8
https://www.youtube.com/watch?v=AbQWlOVlAFE
https://www.youtube.com/watch?v=lkGlLfon2s8
https://www.youtube.com/watch?v=0hD43U1FgGc
https://www.youtube.com/watch?v=MMcJBSBoe24
https://www.youtube.com/watch?v=nVGHioAhCfs
https://www.youtube.com/watch?v=nfcreMsc6_s
https://www.youtube.com/watch?v=OMZUCaz12zI
https://www.youtube.com/watch?v=8lgkbLcxQdo
https://www.youtube.com/watch?v=PGpgNBS6sP0
https://www.youtube.com/watch?v=V1Y-ThisbOU
https://www.youtube.com/watch?v=kJkl9GOkXHc
https://www.youtube.com/watch?v=UOpcx3fa_94
https://www.youtube.com/watch?v=qylSJ5tQ6GQ
https://www.youtube.com/watch?v=C7LaBEyzCeI
https://www.youtube.com/watch?v=0_xwB-k_Svs
https://www.youtube.com/watch?v=a2Wo7ZE_FEI
https://www.youtube.com/watch?v=EzHt0ue76Gk
https://www.youtube.com/watch?v=XFld-6sv1-0
https://www.youtube.com/watch?v=xkYzlf1Rek4
https://www.youtube.com/watch?v=ksotDL6s55k
https://www.youtube.com/watch?v=bBCpIZy6ks4
https://www.youtube.com/watch?v=Xb9WKg7PCn4
https://www.youtube.com/watch?v=w5YhOVH-fgE
https://www.youtube.com/watch?v=iVHz0KjhAQo
https://www.youtube.com/watch?v=oeyzu_rHVQg
https://www.youtube.com/watch?v=6EO41f4yjOk
https://www.youtube.com/watch?v=9vkOuP91_hw
https://www.youtube.com/watch?v=kv-zlYQ4Pb4
https://www.youtube.com/watch?v=MpzPtApxuM0
https://www.youtube.com/watch?v=KJgxgDXOTBc
https://www.youtube.com/watch?v=Q0nhuzrItW8
https://www.youtube.com/watch?v=rwflKElH2W8
https://www.youtube.com/watch?v=0oxPZigpQPE
https://www.youtube.com/watch?v=9b1Fmd0ExZk
https://www.youtube.com/watch?v=CeRb5iuT6ds
https://www.youtube.com/watch?v=ljpvAfdk_TI
https://www.youtube.com/watch?v=EtA3vT_oIH4
https://www.youtube.com/watch?v=JXWwmHpi4fM
https://www.youtube.com/watch?v=wjSjWkBxCEc
https://www.youtube.com/watch?v=bUq3jBX4Cww
https://www.youtube.com/watch?v=GSWuaTSKdQA
https://www.youtube.com/watch?v=u5mp6dUW0Z0
https://www.youtube.com/watch?v=FAxtmdXw7s8
https://www.youtube.com/watch?v=1PIBlJqdEco
https://www.youtube.com/watch?v=DsyjJwr0PNQ
https://www.youtube.com/watch?v=Itr_6Gt26F4
https://www.youtube.com/watch?v=y9jAkmBPvbQ
https://www.youtube.com/watch?v=yqBhe__GSHU
https://www.youtube.com/watch?v=D6WovwRLND8
https://www.youtube.com/watch?v=erLanyOGSks
https://www.youtube.com/watch?v=yN9HDtZOhTc
https://www.youtube.com/watch?v=4szPN4pjI4w
https://www.youtube.com/watch?v=F3aFohOSKpc
https://www.youtube.com/watch?v=nnUyUB5qud8
https://www.youtube.com/watch?v=w55g7DQ7GQw
https://www.youtube.com/watch?v=0dRBFxbgCmc
https://www.youtube.com/watch?v=Ja9jNxG_aLw
https://www.youtube.com/watch?v=dHvOZdeOElc
https://www.youtube.com/watch?v=BNRMIDJT_4g
https://www.youtube.com/watch?v=wIB4z-48kag
https://www.youtube.com/watch?v=OfP55feDfzg
https://www.youtube.com/watch?v=DujkMv1cbdY
https://www.youtube.com/watch?v=BKwvkZ0VIsM
https://www.youtube.com/watch?v=Rcmh4-ZGlks
https://www.youtube.com/watch?v=tGwiQD42beo
https://www.youtube.com/watch?v=VJYrFETv5uA
https://www.youtube.com/watch?v=LovZMI3rp0M
https://www.youtube.com/watch?v=yIfV29IAMJE
https://www.youtube.com/watch?v=bo1zubUlJVA
https://www.youtube.com/watch?v=RhGFg9pHtrs
https://www.youtube.com/watch?v=msQxohScsq0
https://www.youtube.com/watch?v=Tz3VFkhFY5k
https://www.youtube.com/watch?v=rwKKs9PoYdY
https://www.youtube.com/watch?v=D4Y-60t61nA
https://www.youtube.com/watch?v=x7mA9NN8JOI
https://www.youtube.com/watch?v=H4oHRoDYK84
https://www.youtube.com/watch?v=f0vApfuFRUc
https://www.youtube.com/watch?v=WmyctwLYirw
https://www.youtube.com/watch?v=byRzkScw93w
https://www.youtube.com/watch?v=i36EBO4D-Ho
https://www.youtube.com/watch?v=KdO3yWwQnJ0
https://www.youtube.com/watch?v=34IlHei5pqk
https://www.youtube.com/watch?v=A-I-QhoTPxw
https://www.youtube.com/watch?v=Wgfx7gaYZdE
https://www.youtube.com/watch?v=waQ1DmRzNow
https://www.youtube.com/watch?v=yyqQWj0mcD4
https://www.youtube.com/watch?v=6yddHQEticM
https://www.youtube.com/watch?v=L_i9VWErCcQ
https://www.youtube.com/watch?v=TJzcX1SYaL8
https://www.youtube.com/watch?v=jwiEWTVlaIs
https://www.youtube.com/watch?v=VKyuEU5_Od8
https://www.youtube.com/watch?v=55gmDZkJ-Bw
https://www.youtube.com/watch?v=J53UBXIiqXM
https://www.youtube.com/watch?v=CZz69WwxO2w
https://www.youtube.com/watch?v=srVc6eMPrQo
https://www.youtube.com/watch?v=ePYV5CsKP74
https://www.youtube.com/watch?v=P6SyvBdGZ48
https://www.youtube.com/watch?v=hdcHSZK5l_8
https://www.youtube.com/watch?v=BQege4041O8
https://www.youtube.com/watch?v=rA1fTOrfspY
https://www.youtube.com/watch?v=FJ967AaZkmE
https://www.youtube.com/watch?v=U6SGT9Tvcqo
https://www.youtube.com/watch?v=BpRiU4o3H5M
https://www.youtube.com/watch?v=s_E0kalOD-0
https://www.youtube.com/watch?v=8TcmGkiuZ2U
https://www.youtube.com/watch?v=QUqpy0ESbsI
https://www.youtube.com/watch?v=9Ksz3r8xaJ8
https://www.youtube.com/watch?v=M-vn4C9VvKE
https://www.youtube.com/watch?v=89SbAx7p5Sc
https://www.youtube.com/watch?v=_l3kFQkyeQY
https://www.youtube.com/watch?v=D9IHveLAPK0
https://www.youtube.com/watch?v=iyMmVUXIxfY
https://www.youtube.com/watch?v=WGMfTkCodT4
https://www.youtube.com/watch?v=4e6BSO44aeU
https://www.youtube.com/watch?v=qluEmW4ox3A
https://www.youtube.com/watch?v=0Zkk4K_-iso
https://www.youtube.com/watch?v=1MUdPoO1PJ0
https://www.youtube.com/watch?v=r85O2VmXZMQ
https://www.youtube.com/watch?v=mnwsVkznhcE
https://www.youtube.com/watch?v=Id5ktmw_EL0
https://www.youtube.com/watch?v=hiWL9d5_GuU
https://www.youtube.com/watch?v=m3nng0MsBRk
https://www.youtube.com/watch?v=D6mmCQ5UQ3w
https://www.youtube.com/watch?v=tlXbD_auUVk
https://www.youtube.com/watch?v=v9vGd8vnRfU
https://www.youtube.com/watch?v=cTWSftPQq1g
https://www.youtube.com/watch?v=O4_w_nFtvJU
https://www.youtube.com/watch?v=qzoapcR6wQo
https://www.youtube.com/watch?v=V0BhJL0ou1k
https://www.youtube.com/watch?v=B5QTG5__4EE
https://www.youtube.com/watch?v=Asp64fRDStg
https://www.youtube.com/watch?v=XpqiHPUjmDg
https://www.youtube.com/watch?v=fa2LMLBGU9w
https://www.youtube.com/watch?v=Pjo-_3AySEQ
https://www.youtube.com/watch?v=iww6zweVo8w
https://www.youtube.com/watch?v=u8HRFkT89eU
https://www.youtube.com/watch?v=uNS72f3MD0U
https://www.youtube.com/watch?v=1AJ4iH19S58
https://www.youtube.com/watch?v=nQvF0yBwRyM
https://www.youtube.com/watch?v=WOXYWhwzfzw
https://www.youtube.com/watch?v=wNFxh7IYbOM
https://www.youtube.com/watch?v=XEs2pKJvz94
https://www.youtube.com/watch?v=j80Bjy1al-g
https://www.youtube.com/watch?v=dDznnDJ_zyA
https://www.youtube.com/watch?v=5mW98WQP_Wc
https://www.youtube.com/watch?v=DcACit8zwr0
https://www.youtube.com/watch?v=JwjDH96s8_g
https://www.youtube.com/watch?v=sPr1f6ATwVc
https://www.youtube.com/watch?v=G1HOz-O9uXo
https://www.youtube.com/watch?v=hy3lPhICXZw
https://www.youtube.com/watch?v=WJvihhSs36M
https://www.youtube.com/watch?v=goH4f9PYwfU
https://www.youtube.com/watch?v=lru8Gm3cYPE
https://www.youtube.com/watch?v=n5ZNObu2tbA
https://www.youtube.com/watch?v=tGoGcBWhM6c
https://www.youtube.com/watch?v=pbEPvvvFiCA
https://www.youtube.com/watch?v=-EUnSlGvyQ4
https://www.youtube.com/watch?v=Xk8oxQU1_Ck
https://www.youtube.com/watch?v=ugwXthhv8XM
https://www.youtube.com/watch?v=X-uU-NUUxq8
https://www.youtube.com/watch?v=5v_XGYLCrtU
https://www.youtube.com/watch?v=7Ca4X7WtFhM
https://www.youtube.com/watch?v=cmmEuR_5GZc
https://www.youtube.com/watch?v=4Vl4MslyYPE
https://www.youtube.com/watch?v=vpM72XYFmV4
https://www.youtube.com/watch?v=kwXA5flrdqQ
https://www.youtube.com/watch?v=bJOgFMYkTtU
https://www.youtube.com/watch?v=cJuCw3As3eI
https://www.youtube.com/watch?v=eq-9HuDdpOs
https://www.youtube.com/watch?v=sGNxbFfgQXE
https://www.youtube.com/watch?v=5Ich2quTjq0
https://www.youtube.com/watch?v=fx3zF0ZtSig
https://www.youtube.com/watch?v=HgRuF_yTYJQ
https://www.youtube.com/watch?v=5MijUABfB8U
https://www.youtube.com/watch?v=hBpDh8LAcUg
https://www.youtube.com/watch?v=k5Ndhq38MVQ
https://www.youtube.com/watch?v=uOMghPMCRKo
https://www.youtube.com/watch?v=zni_Mj7C62U
https://www.youtube.com/watch?v=pBpq2LS50ns
https://www.youtube.com/watch?v=2Rl0NOQsZ-o
https://www.youtube.com/watch?v=eQv3xdGcRIw
https://www.youtube.com/watch?v=7e3o0chfmFQ
https://www.youtube.com/watch?v=6L_SqAIIr60
https://www.youtube.com/watch?v=fNdCXspZCV8
https://www.youtube.com/watch?v=-OIP0xoAtjA
https://www.youtube.com/watch?v=T_iW9PPn_Mg
https://www.youtube.com/watch?v=a3LUSYqIhDw
https://www.youtube.com/watch?v=ThQTM6V_ufQ
https://www.youtube.com/watch?v=7dvefphQmzE
https://www.youtube.com/watch?v=MCV34cgsdPg
https://www.youtube.com/watch?v=rnwTd7HDcPo
https://www.youtube.com/watch?v=rrXbVZDAw88
https://www.youtube.com/watch?v=WssWMIO4tao
https://www.youtube.com/watch?v=PxqnF1jRNiA
https://www.youtube.com/watch?v=0Cy50nUhgRo
https://www.youtube.com/watch?v=qkBuPBjxu6w
https://www.youtube.com/watch?v=dZquya191Rw
https://www.youtube.com/watch?v=G1W5CKFDeVg
https://www.youtube.com/watch?v=DfrX7KgRIcs
https://www.youtube.com/watch?v=Jmxy_ONeX1E
https://www.youtube.com/watch?v=oWRyOmfrZQE
https://www.youtube.com/watch?v=zoLn4UggHfk
https://www.youtube.com/watch?v=mXPuAhtNuuE
https://www.youtube.com/watch?v=Xnj4j_GoYDw
https://www.youtube.com/watch?v=8Ih0nFOjVu0
https://www.youtube.com/watch?v=99vOCQvmAvY
https://www.youtube.com/watch?v=tBocyMRyMTU
https://www.youtube.com/watch?v=uRIqh5T9Pe4
https://www.youtube.com/watch?v=uEzeFSb0yZ8
https://www.youtube.com/watch?v=xGEsgaL8mwI
https://www.youtube.com/watch?v=5Ts_Dx4v40o
https://www.youtube.com/watch?v=J8sW6csQGe4
https://www.youtube.com/watch?v=aa6HA4kqm8c
https://www.youtube.com/watch?v=BdnbiV0uR9o
https://www.youtube.com/watch?v=o4V0EbJB-rc
https://www.youtube.com/watch?v=ioT3UftDHU8
https://www.youtube.com/watch?v=M2IK9WyI2QE
https://www.youtube.com/watch?v=ixpH3nU2yWw
https://www.youtube.com/watch?v=1iouEfU3opU
https://www.youtube.com/watch?v=XzQ5fa30xQU
https://www.youtube.com/watch?v=otQtl7WLP4I
https://www.youtube.com/watch?v=XQcyDrLILr8
https://www.youtube.com/watch?v=DIbyN6t_UJw
https://www.youtube.com/watch?v=Ib1EfZLVJB4
https://www.youtube.com/watch?v=Vj9ivj1_IxY
https://www.youtube.com/watch?v=1Xw2AUwhChM
https://www.youtube.com/watch?v=rS8jCQcDJEM
https://www.youtube.com/watch?v=RFS9h1fQYZY
https://www.youtube.com/watch?v=KUTd4SlIiHs
https://www.youtube.com/watch?v=FfMK7uXtE0A
https://www.youtube.com/watch?v=jZTawI56cyU
https://www.youtube.com/watch?v=tVRN_rhH5h4
https://www.youtube.com/watch?v=C7a5EQAx8iM
https://www.youtube.com/watch?v=a5FWvlrj02s
https://www.youtube.com/watch?v=cLDzwpUUD48
https://www.youtube.com/watch?v=8it4B9D9mh4
https://www.youtube.com/watch?v=f1HXKFQRZgE
https://www.youtube.com/watch?v=He4hVqpAhcw
https://www.youtube.com/watch?v=h-JxNwc2WvM
https://www.youtube.com/watch?v=-kdt0D3f0o4
https://www.youtube.com/watch?v=A5X4NqcFQXk
https://www.youtube.com/watch?v=s3WE-CcdxNE
https://www.youtube.com/watch?v=srdQXzMk9Ug
https://www.youtube.com/watch?v=t514oMUAxJ8
https://www.youtube.com/watch?v=PjQpV-cno0Q
https://www.youtube.com/watch?v=5BgI3xxw9Wc
https://www.youtube.com/watch?v=Ue0sEbhfpWU
https://www.youtube.com/watch?v=2kp8Hod01A4
https://www.youtube.com/watch?v=2aDOXVaTje0
https://www.youtube.com/watch?v=aDhK7Xhd_mI
https://www.youtube.com/watch?v=Q7u87aVPxs0
https://www.youtube.com/watch?v=Vdf2ccZr8jo
https://www.youtube.com/watch?v=dN1IUOSNxvo
https://www.youtube.com/watch?v=WEY1N-heKAk
https://www.youtube.com/watch?v=97P2QOtGmrM
https://www.youtube.com/watch?v=O1M3r-00J-E
https://www.youtube.com/watch?v=mq5qXQuvz0o
https://www.youtube.com/watch?v=yazqDWnDAIU
https://www.youtube.com/watch?v=stxH-QCUi3A
https://www.youtube.com/watch?v=nUke2nbgtbY
https://www.youtube.com/watch?v=lfKAtdzBaYk
https://www.youtube.com/watch?v=AZEvz0i9ooo
https://www.youtube.com/watch?v=5lr2bpzeF3U
https://www.youtube.com/watch?v=beEAdXJ2ny4
https://www.youtube.com/watch?v=NfBIw1im8pA
https://www.youtube.com/watch?v=PJV6vD9bo74
https://www.youtube.com/watch?v=Qz0fLbnwNAo
https://www.youtube.com/watch?v=CDJbSgxbjkM
https://www.youtube.com/watch?v=OyVk2s84Tiw
https://www.youtube.com/watch?v=smZ6S9ZZutI
https://www.youtube.com/watch?v=6Uczu0tZ4pk
https://www.youtube.com/watch?v=EvLUAqybhNc
https://www.youtube.com/watch?v=WgKbQgWsQEA
https://www.youtube.com/watch?v=HOCXtDvfHmU
https://www.youtube.com/watch?v=FVpKLDyEACg
https://www.youtube.com/watch?v=3QdnwwlslHY
https://www.youtube.com/watch?v=cr7hstPoO-s
https://www.youtube.com/watch?v=1eMY-3EZJcE
https://www.youtube.com/watch?v=cvLy3wYGKbA
https://www.youtube.com/watch?v=mED32-Ujla0
https://www.youtube.com/watch?v=RdALSrpTcTg
https://www.youtube.com/watch?v=KdLn-zpPyYU

	'''  # Put your links in here

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
