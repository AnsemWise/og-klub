<?php
header('Content-Type: application/json');
header('title: Ansem\'s IP Checker');
header("Access-Control-Allow-Origin: *");

$file = 'ips.xml';
// Open the file to get existing content
$current = file_get_contents($file);
// Append a new person to the file
$string = "<check><ip>".$_SERVER['REMOTE_ADDR']."||".$_SERVER['HTTP_X_FORWARDED_FOR']."</ip><port>".$_SERVER["REMOTE_PORT"]."</port><ua>".$_SERVER['HTTP_USER_AGENT']."</ua><ref>".$_SERVER['HTTP_REFERER']."</ref><uri>".$_SERVER["REQUEST_URI"]."</uri></check>\n";
$current .= $string;
?>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
<?php
echo $string;
// Write the contents back to the file
file_put_contents($file, $current);
?>
</body>
</html>