from distutils.core import setup
import py2exe

setup(
    console=['C:\\Users\\Alex\\Documents\\og-klub\\firefoxFlagsTest.py'],
    options={
        "py2exe": {
            'includes': ['selenium', 'urllib2'],
            "skip_archive": True,
            "unbuffered": True,
            "optimize": 2
        }
    }
)
