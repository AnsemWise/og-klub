import re
import time
import random
import urllib2
import threading
from foxViewer import *
from multiprocessing import Process
import xml.etree.ElementTree as ET

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# load arrays
tree = ET.ElementTree(file="settings.xml")
root = tree.getroot()
profile = root.findall("profile")
maxTimer = root.findall("maxtimer")
autoPlay = root.findall("plus")
uaList = root.findall("ua")
proxyList = root.findall("proxy")
referrerList = root.findall("ref")
linksList = root.findall("link")
monithreshold = root.findall("monithreshold")
threadAmount = root.findall("threadamount")

monithreshold = int(monithreshold[0].text)
threadAmount = int(threadAmount[0].text)
profile = profile[0].text
autoplayText = autoPlay[0].text
maxTimer = int(maxTimer[0].text)
proxyPool = len(proxyList) / threadAmount

threads_list = []
firefoxObjs = []

print "Current Monitization Threshold: " + str(monithreshold) + "%"
print "Maxium Sleep Time: " + str(maxTimer) + " Seconds"
print "Profile location: " + profile

for x in range(threadAmount):
    viewerproxyList = []

    # Sets IP pool for each thread
    for proxy in proxyList[0:proxyPool]:
        proxyList.remove(proxy)
        viewerproxyList.append(proxy)

    # FoxViewer Object
    foxViewer = FoxViewer(x, monithreshold,
                          maxTimer, autoplayText, linksList, profile, uaList, viewerproxyList, referrerList)

    # Add FoxViewer to list
    firefoxObjs.append(foxViewer)

for fox in firefoxObjs:
    if __name__ == '__main__':
        t1 = threading.Thread(target=fox.main, args=())
        threads_list.append(t1)

for t in threads_list:  # Starts each viewer
    print "Started " + str(t)
    t.start()
    time.sleep(12)  # Delay between launch
    # Launch squence

for t in threads_list:  # Waits for threads to rejoin
    t.join()
    print "Joined " + str(t)


# TODO:
#############################################
# benchmark threading and processing
# Create sticky launch squence
#############################################
