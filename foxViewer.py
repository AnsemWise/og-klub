import re
import time
import random
import urllib2
import threading
import xml.etree.ElementTree as ET

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class FoxViewer(object):

    def __init__(self, instId, monitizationThreshold, timerText, plusText, links, profiletext, userAgents, proxies, referrers):
        # id
        self.instId = instId

        # percentage monitized
        self.monitizationThreshold = monitizationThreshold

        # Max Timer Setting
        self.sleep = timerText

        # adds &autoplay=1
        self.plus = plusText

        # list of links
        self.links = links

        # ff profile location
        self.profile = profiletext

        # list of user agents
        self.userAgents = userAgents

        # list of proxies
        self.proxies = proxies

        # list of referrers
        self.referrers = referrers

        # browser object
        self.browser = ''

        # view counter
        self.viewCounter = 0

        # ad counter
        self.adCounter = 0

        # Grab base firefox profile
        self.profiletext = profiletext

    def main(self):
        while True:
            # Grab base firefox profile
            instanceProfile = webdriver.FirefoxProfile()
            instanceProfile.add_extension(
                self.profiletext + "\extensions\{455D905A-D37C-4643-A9E2-F6FEFAA0424A}.xpi")
            # general
            instanceProfile.set_preference(
                'browser.xul.error_pages.enabled', "false")
            instanceProfile.set_preference(
                'extensions.blocklist.enabled', "false")
            # playback
            instanceProfile.set_preference(
                'media.mediasource.enabled', "true")
            instanceProfile.set_preference(
                'media.gmp-provider.enabled', "true")
            instanceProfile.set_preference(
                'media.mediasource.mp4.enabled', "true")
            instanceProfile.set_preference('media.fragmented-mp4.*', "true")
            instanceProfile.set_preference(
                'media.fragmented-mp4.use-blank-decoder', "true")
            instanceProfile.set_preference(
                'media.mediasource.webm.enabled', "true")
            instanceProfile.set_preference('media.autoplay.enabled', "true")
            instanceProfile.set_preference(
                'media.peerconnection.enabled', "false")
            # performance
            instanceProfile.set_preference(
                'browser.sessionhistory.max_total_viewer', 0)
            instanceProfile.set_preference('network.http.pipelining', "true")
            instanceProfile.set_preference(
                'network.http.proxy.pipelining', "true")
            instanceProfile.set_preference(
                'network.http.pipelining.maxrequests', 20)
            instanceProfile.set_preference(
                'network.http.pipelining.ssl', "false")
            instanceProfile.set_preference('network.http.max-connections', 45)
            instanceProfile.set_preference(
                'network.http.max-connections-per-server', 30)
            instanceProfile.set_preference('nglayout.initialpaint.delay', 0)
            instanceProfile.set_preference('network.dns.disableIPv6', "false")
            instanceProfile.set_preference('content.notify.backoffcount', 5)
            instanceProfile.set_preference('plugin.expose_full_path', "true")
            instanceProfile.set_preference('config.trim_on_minimize', "true")
            instanceProfile.set_preference('content.notify.backoffcount', 5)
            instanceProfile.set_preference('content.notify.interval', 849999)
            instanceProfile.set_preference('gfx.direct2d.disabled', "false")
            instanceProfile.set_preference(
                'layers.acceleration.disabled', "false")
            instanceProfile.set_preference('dom.ipc.processCount', 3)
            # network
            instanceProfile.set_preference(
                'network.proxy.share_proxy_settings', "true")
            instanceProfile.set_preference(
                'network.proxy.type', 1)  # Proxys on = 1

            # userAgent selection from list randomly
            useragentsel = random.choice(self.userAgents)
            useragentsel = useragentsel.text

            # proxy selection from list randomly
            proxysel = random.choice(self.proxies)
            proxysel = proxysel.text
            proxysel = proxysel.split(':', 1)
            port = int(proxysel[1])

            # referrer selector
            referrersel = random.choice(self.referrers)
            referrersel = '@DEFAULT=' + referrersel.text

            # rolls to check
            random.seed()
            refRand = random.random() * 100

            # chooses when to set ref
            if(refRand < 92):
                # sets ref
                instanceProfile.set_preference(
                    'refcontrol.actions', referrersel)

            instanceProfile.set_preference(
                'general.useragent.override', useragentsel)
            instanceProfile.set_preference('network.proxy.ssl_port', port)
            instanceProfile.set_preference('network.proxy.ssl', proxysel[0])
            instanceProfile.set_preference('network.proxy.http_port', port)
            instanceProfile.set_preference('network.proxy.http', proxysel[0])
            # browser Start
            self.browser = webdriver.Firefox(instanceProfile)

            # self.browser setup
            # self.browser = browser
            self.browser.set_window_size(400, 600, windowHandle='current')

            # print 'Browser setting up'
            self.browser.implicitly_wait(30)

            link = random.choice(self.links)
            link = link.text
            self.browser.get("https://www.youtube.com")
            self.browser.delete_all_cookies()
            time.sleep(4)
            self.browser.get("https://www.youtube.com")
            self.browser.get(link + self.plus)
            # print '--------Loop Start--------'
            # Viewer Vars
            rand = 0
            rand2 = 0
            fullWatch = 0
            sleepTime = self.sleep
            link = ""
            goodPageLoad = False
            reloaded = False  # Checks if the browser has reloaded
            adInVideo = False

            # Viewer Loop Start
            # range is the amount of videos before the self.browser closes
            for runCount in range(15):
                sleepTime = self.sleep
                reloaded = False
                adInVideo = False
                goodPageLoad = False

                # Checks to see if the page loaded
                for pageLoaded in range(10):
                    if "playing-mode" in self.browser.page_source:
                        goodPageLoad = True
                        break

                    elif "cued-mode" in self.browser.page_source:
                        goodPageLoad = False
                        break

                    elif "ad-showing" in self.browser.page_source:
                        goodPageLoad = True
                        adInVideo = True
                        break

                    elif pageLoaded >= 8:
                        if reloaded == True:
                            self.browser.quit()
                        self.browser.refresh()
                        reloaded = True

                    time.sleep(1)

                # page loaded properly
                if goodPageLoad == True:
                    # Watches the whole video
                    random.seed()
                    fullWatch = random.random() * 100
                    if "ad-showing" in self.browser.page_source:
                        adInVideo = True

                    # ad loop
                    if adInVideo == True:
                        # might need to add limit
                        count = 0
                        self.adCounter += 1
                        while True:
                            if "ad-showing" in self.browser.page_source:
                                count += 15
                                time.sleep(15)
                                if count > 300:
                                    break
                            else:
                                break
                        # Watch time after ad is finished
                        time.sleep(34)
                    if fullWatch > 92:  # % chance
                        count = 0
                        while True:
                            if "ended-mode" in self.browser.page_source:
                                break
                            if count > 900:
                                break
                            count += 15
                            time.sleep(15)
                    # no ad on this page
                    if adInVideo == False:
                        random.seed()
                        rand = random.random() * 100
                        sleepTime = random.randint(34, int(sleepTime))
                        if rand > self.monitizationThreshold:
                            time.sleep(sleepTime)
                # Every rotation will use the rest of this code
                # page didn't load
                self.viewCounter += 1
                print "Viewer " + str(self.instId + 1) + " has found " + str(self.adCounter) + " ads and watched " + str(self.viewCounter) + " videos."
                # Random roll
                random.seed()
                rand2 = random.random() * 100
                # New video
                link = random.choice(self.links)
                link = link.text
                self.browser.get(link + self.plus)
            self.browser.delete_all_cookies()
            self.browser.quit()
