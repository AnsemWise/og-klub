import re
import time
import random
import urllib2
import threading
import xml.etree.ElementTree as ET

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class FoxManager(object):

    def __init__(self, instId, monitizationThreshold, timerText, plusText, links, profiletext, userAgents, proxies, referrers):
        # adds &autoplay=1
        self.plus = plusText
        # Max Timer Setting
        self.sleep = timerText
        # list of links
        self.links = links
        # list of user agents
        self.userAgents = userAgents
        # list of proxies
        self.proxies = proxies
        # list of referrers
        self.referrers = referrers
        # ff profile location
        self.profile = profiletext
        # percentage monitized
        self.monitizationThreshold = monitizationThreshold
        # browser object
        self.browser = ''
        # id
        self.instId = instId
        # view counter
        self.viewCounter = 0
        # ad counter
        self.adCounter = 0
        # Grab base firefox profile
        # self.profile = webdriver.FirefoxProfile()
        # self.profile.add_extension(
        # profiletext +
        # "\extensions\{455D905A-D37C-4643-A9E2-F6FEFAA0424A}.xpi")
        self.profile = webdriver.FirefoxProfile(profiletext)

    def main(self):
        while True:
            instanceProfile = self.profile
            # general
            instanceProfile.set_preference(
                'browser.xul.error_pages.enabled', "false")
            instanceProfile.set_preference(
                'extensions.blocklist.enabled', "false")
            # playback
            instanceProfile.set_preference(
                'media.mediasource.enabled', "true")
            instanceProfile.set_preference(
                'media.gmp-provider.enabled', "true")
            instanceProfile.set_preference(
                'media.mediasource.mp4.enabled', "true")
            instanceProfile.set_preference('media.fragmented-mp4.*', "true")
            instanceProfile.set_preference(
                'media.fragmented-mp4.use-blank-decoder', "true")
            instanceProfile.set_preference(
                'media.mediasource.webm.enabled', "true")
            instanceProfile.set_preference('media.autoplay.enabled', "true")
            instanceProfile.set_preference(
                'media.peerconnection.enabled', "false")
            # performance
            instanceProfile.set_preference(
                'browser.sessionhistory.max_total_viewer', 0)
            instanceProfile.set_preference('network.http.pipelining', "true")
            instanceProfile.set_preference(
                'network.http.proxy.pipelining', "true")
            instanceProfile.set_preference(
                'network.http.pipelining.maxrequests', 20)
            instanceProfile.set_preference(
                'network.http.pipelining.ssl', "false")
            instanceProfile.set_preference('network.http.max-connections', 45)
            instanceProfile.set_preference(
                'network.http.max-connections-per-server', 30)
            instanceProfile.set_preference('nglayout.initialpaint.delay', 0)
            instanceProfile.set_preference('network.dns.disableIPv6', "false")
            instanceProfile.set_preference('content.notify.backoffcount', 5)
            instanceProfile.set_preference('plugin.expose_full_path', "true")
            instanceProfile.set_preference('config.trim_on_minimize', "true")
            instanceProfile.set_preference('content.notify.backoffcount', 5)
            instanceProfile.set_preference('content.notify.interval', 849999)
            instanceProfile.set_preference('gfx.direct2d.disabled', "false")
            instanceProfile.set_preference(
                'layers.acceleration.disabled', "false")
            instanceProfile.set_preference('dom.ipc.processCount', 3)
            # network
            instanceProfile.set_preference(
                'network.proxy.share_proxy_settings', "true")
            # CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE CHANGE
            # V
            instanceProfile.set_preference('network.proxy.type', 1)

            # userAgents
            ansemrocks = random.choice(self.userAgents)
            useragentsel = ansemrocks.text

            # proxy selector
            money = random.choice(self.proxies)
            proxysel = money.text
            proxysel = proxysel.split(':', 1)
            # print proxysel[0] + " " + proxysel[1]
            port = int(proxysel[1])
            # proxies.remove(money)

            # chooses when to set ref
            random.seed()
            rand = random.random() * 100
            refrand = random.random() * 100

            referrer = ""
            # chooses when to set ref
            if(rand < 92):
                # sets ref
                if(refrand < 30):
                    referrer = "Facebook"
                    instanceProfile.set_preference(
                        'refcontrol.actions', '@DEFAULT=https://facebook.com')
                elif refrand >= 30 and refrand < 70:
                    referrer = "Twitter"
                    instanceProfile.set_preference(
                        'refcontrol.actions', '@DEFAULT=https://t.co/ppvGXuMQds')
                elif refrand >= 70:
                    referrer = "Google"
                    instanceProfile.set_preference(
                        'refcontrol.actions', '@DEFAULT=https://www.google.com/')

            instanceProfile.set_preference(
                'general.useragent.override', useragentsel)
            instanceProfile.set_preference('network.proxy.ssl_port', port)
            instanceProfile.set_preference('network.proxy.ssl', proxysel[0])
            instanceProfile.set_preference('network.proxy.http_port', port)
            instanceProfile.set_preference('network.proxy.http', proxysel[0])
            # browser Start
            self.browser = webdriver.Firefox(instanceProfile)

            # self.browser setup
            # self.browser = browser
            self.browser.set_window_size(300, 200, windowHandle='current')

            # print 'Browser setting up'
            self.browser.implicitly_wait(30)
            if refrand >= 30 and refrand < 70:
                self.browser.get(
                    "https://twitter.com/kirstenfoley22/status/694316812142559232")
                time.sleep(4)
                self.browser.get("https://t.co/ppvGXuMQds")
            else:
                self.browser.get(
                    "http://www.buzzfeed.com/kaf10/too-real-for-women-who-use-public-bathrooms-24n1a")

            random.seed()
            sleepTime = random.randint(128, 342)
            self.viewCounter += 1
            print referrer + " view | Viewer " + str(self.instId + 1) + " has viewed the article " + str(self.viewCounter) + " times | Sleeping for " + str(sleepTime)
            self.browser.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(sleepTime)
            self.browser.quit()
