import re
import time
import random
import urllib2
import threading
from foxViewer import *
from multiprocessing import Process
import xml.etree.ElementTree as ET

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# load arrays
tree = ET.ElementTree(file="settings.xml")
root = tree.getroot()
profile = root.findall("profile")
timerText = root.findall("maxtimer")
plusText = root.findall("plus")
userAgents = root.findall("ua")
proxies = root.findall("proxy")
referrers = root.findall("ref")
links = root.findall("link")
monthreshold = root.findall("monthreshold")
viewerAmount = root.findall("viewerAmount")

monthreshold = int(monthreshold[0].text)
viewerAmount = int(viewerAmount[0].text)
profile = profile[0].text
plus = plusText[0].text
timerText = int(timerText[0].text)
pool = len(proxies) / viewerAmount

threads_list = []
firefoxObjs = []

for view in range(viewerAmount):
    viewerproxies = []
    for proxy in proxies[0:pool]:
        proxies.remove(proxy)
        viewerproxies.append(proxy)
    print str(len(viewerproxies))
    foxManager = FoxManager(view, monthreshold,
                            timerText, plus, links, profile, userAgents, viewerproxies, referrers)
    firefoxObjs.append(foxManager)

for fox in firefoxObjs:
    if __name__ == '__main__':
        t1 = threading.Thread(target=fox.main, args=())
        threads_list.append(t1)

for t in threads_list:
    print "Started " + str(t)
    t.start()
    time.sleep(12)

for t in threads_list:
    t.join()
    print "Joined " + str(t)


# TODO:
#############################################
# benchmark threading and processing
#
#############################################
